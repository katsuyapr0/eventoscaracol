//
//  MenuTableViewCell.h
//  EventosCaracol
//
//  Created by Developer on 26/11/13.
//  Copyright (c) 2013 iAmStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell
@property (strong, nonatomic) UILabel *menuItemLabel;
@property (strong, nonatomic) UIImageView *menuItemImageView;
@end
