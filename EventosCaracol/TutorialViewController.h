//
//  TutorialViewController.h
//  EventosCaracol
//
//  Created by Developer on 29/11/13.
//  Copyright (c) 2013 iAmStudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"

@interface TutorialViewController : UIViewController <UIScrollViewDelegate>
@property (nonatomic) BOOL tutorialWasPresentedFromSideMenu;
@end
