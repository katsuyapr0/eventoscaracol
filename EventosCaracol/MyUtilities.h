//
//  MyUtilities.h
//  EventosCaracol
//
//  Created by Developer on 16/01/14.
//  Copyright (c) 2014 iAmStudio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyUtilities : NSObject
+(UIImage *)imageWithName:(UIImage *)image ScaleToSize:(CGSize)newSize;
@end