//
//  DestacadosCollectionViewCell.m
//  EventosCaracol
//
//  Created by Developer on 22/11/13.
//  Copyright (c) 2013 iAmStudio. All rights reserved.
//

#import "DestacadosCollectionViewCell.h"

@implementation DestacadosCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        
        self.featuredEventNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0,
                                                                           self.contentView.bounds.size.height - 60.0,
                                                                           self.contentView.bounds.size.width - 20,
                                                                           self.contentView.bounds.size.height - (self.contentView.bounds.size.height - 60.0))];
        self.featuredEventNameLabel.numberOfLines = 0;
        self.featuredEventNameLabel.textAlignment = NSTextAlignmentLeft;
        self.featuredEventNameLabel.textColor = [UIColor whiteColor];
        self.featuredEventNameLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:13.0];
        self.featuredEventImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                                    0,
                                                                                    self.contentView.bounds.size.width,
                                                                                    self.contentView.bounds.size.height)];
        [self.featuredEventImageView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self.featuredEventImageView setContentMode:UIViewContentModeScaleAspectFill];
        self.featuredEventImageView.clipsToBounds = YES;
        
        UIView *patternView = [[UIView alloc] initWithFrame:self.contentView.frame];
        UIImage *patternImage = [UIImage imageNamed:@"Pattern.png"];
        patternImage = [MyUtilities imageWithName:patternImage ScaleToSize:CGSizeMake(1.0, self.frame.size.height)];
        patternView.backgroundColor = [UIColor colorWithPatternImage:patternImage];
        
        /*self.spinner = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(self.contentView.frame.size.width/2 - 25.0,
                                                                                 self.contentView.frame.size.height/2 - 25.0,
                                                                                 50.0,
                                                                                 50.0)];
        self.spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;*/
        
        self.contentView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.featuredEventImageView];
        [self.contentView addSubview:patternView];
        [self.contentView addSubview:self.featuredEventNameLabel];
        [self.contentView addSubview:self.spinner];
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}


@end
