//
//  BannerView.h
//  EventosCaracol
//
//  Created by Developer on 7/02/14.
//  Copyright (c) 2014 iAmStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BannerView : UIView
+(void)showBannerOverView:(UIView *)view withImage:(UIImage *)image;
@end
